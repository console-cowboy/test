import React from "react";
import Button from "../Button";


function StartScreen() {
    return (
        <div className="start-screen">
              <Button />
        </div>
    );
}

export default StartScreen;