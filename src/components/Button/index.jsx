import React from "react";
import { ButtonStyled } from "./styled";

function Button() {
    return (
    <ButtonStyled className="start-btn">
      Start Playing!
    </ButtonStyled>
    )
};

export default Button;