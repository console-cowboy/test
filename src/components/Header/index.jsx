import React from "react";
import  { HeaderStyled }  from "./styled.jsx";

function Header() {
    return (
        <HeaderStyled>
        <h1>MEOWERY</h1>
        <h2>Meeeow, find two cats that look the same!</h2>
        </HeaderStyled>
    )

}

export default Header;