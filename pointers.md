# Pointers

PS. Fan vilket fint spel!!😻 Älskar!

---

## Ändra Mappstrukturen

- Ert repo-namn motsvarar "mappnamnet" som skapas när repot laddas/klonas ner.
  Strukturen blir då:

```
└── Meowery # Titeln på ert GitHub-projekt.
    ├── meowery 📌 # En mapp ni placerat projektet i.
    │   ├── ... Resten av appen.
```

Hade börjat med att flytta ut allt innehåll från meowery (📌) så att innehållet
istället hamnar i "föräldramappen" "Meowery". Strukturen för projektet blir lite
oväntad/wonky annars.😊

Då kommer ni också kunna köra `npm install` etc från roten av projektet. Nåt man
ofta förväntar sig kunna göra i Node-baserade projektet.

---

## Hur Jag Personligen Hade Lagt Upp Arbetet

> "just nu är problemet att om man klickar på en bild så visas den matchande
> bilden också.. förstår inte var det blir fel"

Hur jag personligen hade lagt upp arbetet:

1. Ändrat mappstrukturen (se ovan).👆 _Tips!_ Enklast är att skrota sitt repo
   till papperskorgen, klona ner på nytt och ändra placeringen av filerna och
   **sist** köra `npm i` när allt är klart.

- Hade personligen låtit bli att manipulera `public`-mappen (även om det
  funkar). Det är ett lite känsligt område för all React-logik. Hade kört på ett
  säkert kort och gått på en ganska vanlig konvention att skapa upp en
  `assets`-mapp och placerat alla bilder där istället. Lite tryggare och lite
  mer "väntat".

2. Förenkla debugging av problemet genom att först plocka bort logik som inte
   används (ex `components/Board/board.jsx`).

3. Aktiverat en debugger i VSCode! Debuggers är era bästa vänner i såna här
   lägen, så slipper man `console.log`'a gång på gång. Förstår om det känns
   överkurs men vill varmt rekommendera att testa:
   https://www.youtube.com/watch?v=PJeNReqyH88

Går 100x snabbare att få bukt på luriga buggar när man väl kommit igång.

4. Fått till ett funktionellt flöde _utan_ att hämta in katter nödvändigtvis.
   Hade testat lokalt med matchande färger för att förenkla och bytt ut färgerna
   mot katterna som sista fix.

5. Styckat upp logiken i flera sektioner (försvårar annars att kunna pin-point'a
   vart ett problem uppstår när det väl smäller). När någonting inte funkar är
   det bra att börja skala ner och kommentera ut kod istället för att lägga till
   ytterligare. Hade övervägt att omvandla koden till olika hjälpfunktioner, ex:

   1. `getCatImages` - returnerar bilderna som ska användas.
   2. `shuffleDeck` - tar in arrayerna av duplicateImages och togglas vid
      knapptryck av `New Game`-knappen.

6. Sist men inte minst hade jag följt detta: En mkt bra utförlig
   tutorial-spellista på hur man kan skapa ett memory-spel i React (tror ni kan
   börja kolla fr.o.m. video 2):
   https://www.youtube.com/watch?v=ZCKohZwGZMw&list=PL4cUxeGkcC9iQ7g2eoNXHCJBBBz40S_Lm
